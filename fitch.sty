%% fitch.sty -- Fitch-style proofs in LaTeX.
%% Copyright (C) 2015-2018 Guilherme F. Lima
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status 'maintained'.
%
% The Current Maintainer of this work is Guilherme F. Lima.
%
% This work consists of the files fitch.sty and fitch.tex.
\ProvidesPackage{fitch}
\NeedsTeXFormat{LaTeX2e}
\DeclareOption{abbrev}{
  \let\iff\leftrightarrow
  \def\botI,#1,#2{$\bot$I,~#1,~#2}
  \def\botE,#1{$\bot$E,~#1}
  \def\notI,#1-#2{$\lnot$I,~#1--#2}
  \def\notE,#1{$\lnot$E,~#1}
  \def\andI,#1,#2{$\land$I,~#1,~#2}
  \def\andE,#1{$\land$E,~#1}
  \def\orI,#1{$\lor$I,~#1}
  \def\orE,#1,#2-#3,#4-#5{$\lor$E,~#1,~#2--#3,~#4--#5}
  \def\toE,#1,#2{$\to$E,~#1,~#2}
  \def\toI,#1-#2{$\to$I,~#1--#2}
  \def\iffI,#1-#2,#3-#4{$\iff$I,~#1--#2,~#3--#4}
  \def\iffE,#1,#2{$\iff$E,~#1,~#2}
  \def\forallI,#1-#2{$\forall$I,~#1--#2}
  \def\forallE,#1{$\forall$E,~#1}
  \def\existsI,#1{$\exists$I,~#1}
  \def\existsE,#1,#2-#3{$\exists$E,~#1,~#2--#3}
  \def\existsiI,#1,#2-#3{$\exists!$I,~#1,~#2--#3}
  \def\eqI{$=$I}
  \def\eqE,#1,#2{$=$E,~#1,~#2}
  \def\R,#1{R,~#1}
}
\DeclareOption*{
  \ClassWarning{quotes}{Unknown option '\CurrentOption'}
}
\ProcessOptions\relax
\RequirePackage{amsmath}
\RequirePackage{environ}

\newcount\fitch@line            % current line number
\newcount\fitch@level           % current nesting level
\def\fitch@openvar{}            % current open variable
\def\fitch@buf{\@empty}         % line buffer

\newdimen\fitch@tab             % space between vertical lines
\setbox0=\hbox{$\,\bot\,$}      % (make sure that they align with \bot)
\fitch@tab=\wd0

% Resets line buffer and counters.
\def\fitch@reset{%
  \fitch@line=0
  \fitch@level=0
  \gdef\fitch@openvar{}%
  \gdef\fitch@buf{\@empty}%
}

% Pushes #1 to line buffer.
% (Internal macro used by \fitch@push.)
\toksdef\fitch@ta=0
\toksdef\fitch@tb=2
\long\def\fitch@dopush#1{%
  \fitch@ta={\\{#1}}%
  \fitch@tb=\expandafter{\fitch@buf}%
  \edef\fitch@buf{\the\fitch@tb\the\fitch@ta}%
}

% Pushes line to line buffer.
%   #1: type ('hypo' or 'have');
%   #2: label;
%   #3: formula;
%   #4: justification.
\def\fitch@push#1#2#3#4{%
  \fitch@ifempty{#2}%
    {\advance\fitch@line by1\relax}%
    {}%
  \protected@edef\fitch@@push{%
    \noexpand
    \fitch@dopush{%
      {\the\fitch@line}{\the\fitch@level}{\fitch@openvar}{#1}{#2}{#3}{#4}%
    }%
  }%
  \fitch@@push
}

% Get line components.
\def\fitch@getline#1#2#3#4#5#6#7{#1}
\def\fitch@getlevel#1#2#3#4#5#6#7{#2}
\def\fitch@getopt#1#2#3#4#5#6#7{#3}
\def\fitch@gettype#1#2#3#4#5#6#7{#4}
\def\fitch@getlabel#1#2#3#4#5#6#7{#5}
\def\fitch@getform#1#2#3#4#5#6#7{#6}
\def\fitch@getjust#1#2#3#4#5#6#7{#7}

% Expands to #1 occurrences of #2.
\def\fitch@rep#1#2{%
  \count0=#1\relax
  \loop
    \ifnum\count0>0%
      #2\relax
      \advance\count0by-1
  \repeat
}

% If #1 expands to nothing, then expands to #2; otherwise expands to #3.
\def\fitch@ifempty#1#2#3{%
  \setbox0=\hbox{#1}%
  \ifdim\wd0=0pt#2\else#3\fi
}

% User commands.
\def\fitch@open#1{\gdef\fitch@openvar{#1}\advance\fitch@level1\ignorespaces}
\def\fitch@close{\advance\fitch@level-1\ignorespaces}
\def\fitch@hypo#1#2#3{\fitch@push{hypo}{#1}{#2}{#3}\ignorespaces}
\def\fitch@have#1#2#3{\fitch@push{have}{#1}{#2}{#3}\ignorespaces}

% Typesets the contents of line buffer.
% (Internal macro used \fitch@typeset.)
\def\fitch@dotypeset#1{%
  \long\def\\##1{%
    % label
    \strut
    \fitch@ifempty{\fitch@getlabel##1}%
      {\ifx#1p\fitch@getline##1\,\fi}%
      {\fitch@getlabel##1\,}&
    % formula
    \fitch@rep{\fitch@getlevel##1}{\vrule\hskip.4875\fitch@tab}%
    \expandafter\ifx\csname\fitch@gettype##1\endcsname\hypo% \hypo?
      \strut\vrule height.5\ht\strutbox
      \vtop{%
        \hbox{%
          \strut
          \fitch@ifempty{\fitch@getopt##1}{}{%
            \setlength{\fboxsep}{1pt}%
            \,\fbox{\fitch@getopt##1}%
          }%
          \fitch@ifempty{\fitch@getform##1}{}{\,\fitch@getform##1}%
        }%
        \hrule
      }%
    \else
      \strut\vrule\fitch@ifempty{\fitch@getform##1}{}{\,\fitch@getform##1}%
    \fi
    \edef\fitch@@dotypeset{%
      \ifx#1p\span\else&\fi
    }%
    \fitch@@dotypeset
    % justification
    \fitch@ifempty{\fitch@getjust##1}{}{%
      \ifx#1p\relax
        \leaders\hbox to2em{\hss.\hss}\hfill%
      \else
        \quad
      \fi
      \fitch@getjust##1\relax
    }%
    \cr
  }%
  \ifx#1p%
    {\offinterlineskip
    \halign to\hsize{\hfil##&\tabskip0pt plus1fil##&##\tabskip0pt\cr
      &&\cr
      \fitch@buf
    }}%
  \else
    \hbox{%
      \vtop{%
        \tabskip0pt
        \offinterlineskip
        \halign{\hfil##&##&##\cr
          \fitch@buf
        }%
      }%
    }%
  \fi
}

% Typesets a Fitch-style proof.
%   #1: type ('p' for paragraph or 't' for tree).
\def\fitch@typeset#1#2{%
  \begingroup
    \newcommand*\open[1][]{\fitch@open{##1}}%
    \newcommand*\close{\fitch@close}%
    \newcommand*\hypo[3][]{\fitch@hypo{##1}{##2}{##3}}%
    \newcommand*\have{\@ifstar{\@@have}{\@have}}%
    \newcommand*\@have[3][]{\fitch@have{##1}{##2}{##3}}%
    \newcommand*\@@have[3][]{\fitch@have{\,}{##2}{##3}}%
    \ignorespaces#2\ignorespaces
    \fitch@dotypeset{#1}\ignorespaces
  \endgroup
}

% The fitch, fitchpar, and fitchproof environments.
\NewEnviron{fitch}[1][t]{%
  \fitch@typeset{#1}{\BODY}}

\NewEnviron{fitchpar}[1][p]{%
  \penalty\predisplaypenalty\vskip\abovedisplayskip
  \fitch@typeset{#1}{\BODY}%
  \penalty\postdisplaypenalty\vskip\belowdisplayskip
  \noindent\ignorespaces
}

\NewEnviron{fitchproof}[1][p]{%
  \begin{proof}
    \mbox{}%
    \begin{fitch}[#1]
      \BODY
    \end{fitch}
  \end{proof}}
\endinput
