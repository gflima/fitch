# fitch
A LaTeX package for typesetting Fitch-style proofs.

For pre-compiled samples, cf.
http://www.telemidia.puc-rio.br/~gflima/fitch/fitch.pdf.
