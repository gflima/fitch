--[[ tests.lua -- Check fitch.lua.
     Copyright (C) 2016 PUC-Rio/Laboratorio TeleMidia

This file is part of Fitch.

Fitch is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

Fitch is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Fitch.  If not, see <http://www.gnu.org/licenses/>.  ]]--

local assert = assert
local error = error
local ipairs = ipairs
local pcall = pcall
local print = print
local tostring = tostring
local type = type

local fitch = require'fitch'
_ENV = nil

local function xfail (f, ...)
   if pcall (f, ...) then
      error ('should fail', 2)
   end
end


-- Contexts.
do
   local C = assert (fitch:new ()) -- new
   assert (C:type () == 'context')
   assert (C:context () == C)
   assert (not C:is_expression ())
   assert (not C:is_formula ())
   assert (not C:is_term ())
   assert (tostring (C) == '')
   assert (C == C)
   assert (C ~= 0)
   assert (C ~= '')
   assert (C ~= {})
   assert (C ~= C:new ())

   local C = fitch:new {        -- new with spec and install
      {'a', 'func', 0, {'α'}, {'\\alpha'}},
      {'b', 'func', 1, {'β'}, {'\\beta'}},
      {'A', 'pred', 2, {'𝔄'}, {'\\mathfrak{A}'}},
      {'B', 'pred', 3, {'𝔅'}, {'\\mathfrak{B}'}, {infix=true}},
   }
   assert (C:install ('c', 'func', 0, {'γ'}, {'\\gamma'}))
   assert (tostring (C) == [[
a^0	func	α	\alpha
b^1	func	β	\beta
c^0	func	γ	\gamma
A^2	pred	𝔄	\mathfrak{A}
B^3	pred	𝔅	\mathfrak{B}
]])

   assert (C:install ('P', 'pred', 2, nil, nil))
   assert (C:install ('Q', 'pred', 2, nil, {'\\Q', '\\q'}, {infix=true}))
   assert (C:install ('R', 'pred', 48))
   assert (C:install ('f', 'func', 2, nil, nil, {infix=true}))
   assert (tostring (C):match [[
a^0	func	α	\alpha
b^1	func	β	\beta
c^0	func	γ	\gamma
f^2%*	func%s*
A^2	pred	𝔄	\mathfrak{A}
B^3	pred	𝔅	\mathfrak{B}
P^2	pred%s*
Q^2%*	pred		\Q,\q
R^48	pred%s*
]] == tostring (C))

   assert (C:type () == 'context')
   assert (C:context () == C)
   assert (not C:is_expression ())
   assert (not C:is_formula ())
   assert (not C:is_term ())
   assert (C == C)
   assert (C ~= {})
   assert (C ~= C:new ())

   xfail (fitch.new, nil, 0)    -- sanity checks
   xfail (fitch.new, nil, 'x')
   local C = fitch:new ()

   xfail (fitch.install, C)
   xfail (fitch.install, C, 'a')
   xfail (fitch.install, C, 'a', 'b')
   xfail (fitch.install, C, 'a', 'func')
   xfail (fitch.install, C, 'a', 'func', -1)
   xfail (fitch.install, C, 'a', 'pred')
   xfail (fitch.install, C, 'a', 'pred', -1)
   assert (C:install ('a', 'func', 1))

   xfail (C.new, C, {{'a'}, {'b'}, {'c'}})
   xfail (C.new, C, {{'a', 'func'}, {'b', 'func'}, {'c', 'pred'}})
   xfail (C.new, C, {{'a', 'func', 1}, {'b', 'func', 1}, {'c', 'pred', -1}})
   assert (C:new {{'a', 'func', 1}, {'b', 'func', 1}, {'c', 'pred', 1}})
end


-- Terms and formulas.
do
   local C = fitch.new {
      -- generic
      {'a', 'func', 0, {'α'}, {'\\alpha'}},
      {'b', 'func', 0, {'β'}, {'\\beta'}},
      {'f', 'func', 1, {'φ'}, {'\\varphi'}},
      {'i', 'func', 1, {'ι'}, {'\\iota'}},
      {'g', 'func', 2, {'ψ'}, {'\\psi'}, {infix=1}},
      {'h', 'func', 2, {'γ'}, {'\\gamma'}, {infix=2}},
      {'u', 'func', 2, {'μ'}, {'\\mu'}, {infix=1}},
      {'w', 'func', 2, {'ω'}, {'\\omega'}, {infix=3}},
      {'t', 'func', 3, {'τ'}, {'\\tau'}},
      {'A', 'pred', 0},
      {'B', 'pred', 0},
      {'P', 'pred', 1},
      {'Q', 'pred', 2},
      {'R', 'pred', 2, nil, nil,{infix=true}},
      {'T', 'pred', 3,},
      -- set theory
      {'empty', 'func', 0, {'∅'}, {'\\varnothing'}},
      {'cap', 'func', 2, {'∩'}, {'\\cap'}, {infix=1}},
      {'cup', 'func', 2, {'∪'}, {'\\cup'}, {infix=2}},
      {'in', 'pred', 2, {'∈'}, {'\\in'}, {infix=true}},
      {'subset', 'pred', 2, {'⊂'}, {'\\subset'}, {infix=true}},
      {'subseteq', 'pred', 2, {'⊆'}, {'\\subseteq'}, {infix=true}},
   }

   local TESTS = {

-------- terms -------------------------------------------------------------
      {expr='x',
       type='term',
       tag='var',
       arity=0,
       rank=0,
       unpack={},
       proper={},
       str='x',
       utf8='x',
       latex='x',
      },
      {expr={'a', 'α', '\\alpha'},
       type='term',
       tag='func',
       arity=0,
       rank=0,
       unpack={},
       proper={},
       str='a',
       utf8='α',
       latex='\\alpha',
      },
      {expr={'fa', 'φ α', '(\\varphi((\\alpha)))'},
       type='term',
       tag='func',
       arity=1,
       rank=0,
       unpack={'a'},
       proper={'a'},
       str='f(a)',
       utf8='φ(α)',
       latex='\\varphi(\\alpha)',
      },
      {expr={'xgx', 'x\\psix'},
       type='term',
       tag='func',
       arity=2,
       rank=0,
       unpack={'x', 'x'},
       proper={'x'},
       str='g(x,x)',
       utf8='xψx',
       latex='x\\psi{x}',
      },
      {expr='fyufy',
       type='term',
       tag='func',
       arity=2,
       rank=0,
       unpack={'f(y)', 'f(y)'},
       proper={'y', 'f(y)'},
       str='u(f(y),f(y))',
       utf8='φ(y)μφ(y)',
       latex='\\varphi(y)\\mu\\varphi(y)',
      },
      {expr='ffffawfa',
       type='term',
       tag='func',
       arity=2,
       rank=0,
       unpack={'f(f(f(f(a))))', 'f(a)'},
       proper={'a', 'fa', 'fffa', 'ffa', 'ffffa'},
       str='w(f(f(f(f(a)))),f(a))',
       utf8='φ(φ(φ(φ(α))))ωφ(α)',
       latex='\\varphi(\\varphi(\\varphi(\\varphi(\\alpha))))'
          ..'\\omega\\varphi(\\alpha)',
      },
      {expr='ffff(awfa)',
       type='term',
       tag='func',
       arity=1,
       rank=0,
       unpack={'f(f(f(w(a,f(a)))))'},
       proper={'a', 'fa', 'awfa', 'f(awfa)', 'fff(awfa)'},
       str='f(f(f(f(w(a,f(a))))))',
       utf8='φ(φ(φ(φ(αωφ(α)))))',
       latex='\\varphi(\\varphi(\\varphi(\\varphi(\\alpha'
          ..'\\omega\\varphi(\\alpha)))))',
      },
      {expr='x g y g z',
       type='term',
       tag='func',
       arity=2,
       rank=0,
       unpack={'x', 'g(y,z)'},
       str='g(x,g(y,z))',
       utf8='xψyψz',
       latex='x\\psi{y\\psi{z}}',
      },
      {expr='xgygzgxgygz',
       type='term',
       tag='func',
       arity=2,
       rank=0,
       unpack={'x','g(y,g(z,g(x,g(y,z))))'},
       str='g(x,g(y,g(z,g(x,g(y,z)))))',
       utf8='xψyψzψxψyψz',
       latex='x\\psi{y\\psi{z\\psi{x\\psi{y\\psi{z}}}}}',
      },
      {expr='xgxuxgx',
       type='term',
       tag='func',
       arity=2,
       rank=0,
       unpack={'g(x,x)', 'g(x,x)'},
       str='u(g(x,x),g(x,x))',
       utf8='xψxμxψx',
       latex='x\\psi{x}\\mu{x\\psi{x}}',
      },
      {expr='xgyhx',
       type='term',
       tag='func',
       arity=2,
       rank=0,
       unpack={'g(x,y)', 'x'},
       str='h(g(x,y),x)',
       utf8='xψyγx',
       latex='x\\psi{y}\\gamma{x}',
      },
      {expr='fxgxuxhfx',
       type='term',
       tag='func',
       arity=2,
       rank=0,
       unpack={'u(g(f(x),x),x)', 'f(x)'},
       str='h(u(g(f(x),x),x),f(x))',
       utf8='φ(x)ψxμxγφ(x)',
       latex='\\varphi(x)\\psi{x}\\mu{x}\\gamma\\varphi(x)',
      },
      {expr='xh(xhx)ux',
       type='term',
       tag='func',
       arity=2,
       rank=0,
       unpack={'x', 'u(h(x,x),x)'},
       str='h(x,u(h(x,x),x))',
       utf8='xγ(xγx)μx',
       latex='x\\gamma(x\\gamma{x})\\mu{x}',
      },
      {expr={'xhy', 'hxy', 'h(x,y)'},
       type='term',
       tag='func',
       arity=2,
       rank=0,
       unpack={'x', 'y'},
       str='h(x,y)',
       utf8='xγy',
       latex='x\\gamma{y}',
      },
      {expr='xgxhf(xgxh(gxg))',
       type='term',
       tag='func',
       arity=2,
       rank=0,
       unpack={'g(x,x)', 'f(h(g(x,x),g(x,g)))'},
       str='h(g(x,x),f(h(g(x,x),g(x,g))))',
       utf8='xψxγφ(xψxγxψg)',
       latex='x\\psi{x}\\gamma\\varphi(x\\psi{x}\\gamma{x\\psi{g}})',
      },
      {expr='xgxhxgxwfx',
       type='term',
       tag='func',
       arity=2,
       rank=0,
       unpack={'h(g(x,x),g(x,x))', 'f(x)'},
       str='w(h(g(x,x),g(x,x)),f(x))',
       utf8='xψxγxψxωφ(x)',
       latex='x\\psi{x}\\gamma{x\\psi{x}}\\omega\\varphi(x)'
      },
      {expr='xg(xh(xgx))wfx',
       type='term',
       tag='func',
       arity=2,
       rank=0,
       unpack={'g(x,h(x,g(x,x)))', 'f(x)'},
       str='w(g(x,h(x,g(x,x))),f(x))',
       utf8='xψ(xγxψx)ωφ(x)',
       latex='x\\psi(x\\gamma{x\\psi{x}})\\omega\\varphi(x)',
      },
      {expr='acapbcupbcapa',
       type='term',
       tag='func',
       arity=2,
       rank=0,
       unpack={'cap(a,b)','cap(b,a)'},
       str='cup(cap(a,b),cap(b,a))',
       utf8='α∩β∪β∩α',
       latex='\\alpha\\cap\\beta\\cup\\beta\\cap\\alpha',
      },
      {expr='xgxhtxgxahaa',
       type='term',
       tag='func',
       arity=2,
       rank=0,
       unpack={'g(x,x)', 't(x,g(x,a),h(a,a))'},
       str='h(g(x,x),t(x,g(x,a),h(a,a)))',
       utf8='xψxγτ(x,xψα,αγα)',
       latex='x\\psi{x}\\gamma\\tau(x,x\\psi\\alpha,\\alpha\\gamma\\alpha)',
      },
      {expr='tffffffxataab',
       type='term',
       tag='func',
       arity=3,
       rank=0,
       unpack={'f(f(f(f(f(f(x))))))', 'a', 't(a,a,b)'},
       str='t(f(f(f(f(f(f(x)))))),a,t(a,a,b))',
       utf8='τ(φ(φ(φ(φ(φ(φ(x)))))),α,τ(α,α,β))',
       latex='\\tau(\\varphi(\\varphi(\\varphi(\\varphi(\\varphi'
          ..'(\\varphi(x)))))),\\alpha,\\tau(\\alpha,\\alpha,\\beta))',
      },

-------- atomic formulas ---------------------------------------------------
      {expr='x=x',
       type='formula',
       tag='eq',
       arity=2,
       rank=0,
       unpack={'x', 'x'},
       str='x=x',
       utf8='x=x',
       latex='x=x',
      },
      {expr={'x=a', 'x   = a', 'x=α()'},
       type='formula',
       tag='eq',
       arity=2,
       rank=0,
       unpack={'x', 'a'},
       str='x=a',
       utf8='x=α',
       latex='x=\\alpha',
      },
      {expr={'((f(x)=g(x,y)))', '(fx)=xgy'},
       type='formula',
       tag='eq',
       arity=2,
       rank=0,
       unpack={'f(x)', 'g(x,y)'},
       str='f(x)=g(x,y)',
       utf8='φ(x)=xψy',
       latex='\\varphi(x)=x\\psi{y}',
      },
      {expr={'Px', 'P(x)', '(P((x)))'},
       type='formula',
       tag='pred',
       arity=1,
       rank=0,
       unpack={'x'},
       str='P(x)',
       utf8='P(x)',
       latex='P(x)',
      },
      {expr={'Pα', 'P ((a))', '(((P   a)))'},
       type='formula',
       tag='pred',
       arity=1,
       rank=0,
       unpack={'a'},
       str='P(a)',
       utf8='P(α)',
       latex='P(\\alpha)',
      },
      {expr={'Pfffx', 'P(f(f(f(x))))', '(P(f f f((( x )))))'},
       type='formula',
       tag='pred',
       arity=1,
       rank=0,
       unpack={'f(f(f(x)))'},
       str='P(f(f(f(x))))',
       utf8='P(φ(φ(φ(x))))',
       latex='P(\\varphi(\\varphi(\\varphi(x))))',
      },
      {expr={'Pffgbfx', '  P  (ffg(b,fx))'},
       type='formula',
       tag='pred',
       arity=1,
       rank=0,
       unpack={'f(f(g(b,f(x))))'},
       str='P(f(f(g(b,f(x)))))',
       utf8='P(φ(φ(βψφ(x))))',
       latex='P(\\varphi(\\varphi(\\beta\\psi\\varphi(x))))',
      },
      {expr={'Ptabx', 'Pt(a,b,x)', 'P((((((((tabx))))))))'},
       type='formula',
       tag='pred',
       arity=1,
       rank=0,
       unpack={'t(a,b,x)'},
       str='P(t(a,b,x))',
       utf8='P(τ(α,β,x))',
       latex='P(\\tau(\\alpha,\\beta,x))',
      },
      {expr={'Qxy', 'Q(x,y)', 'Q(x)(y)'},
       type='formula',
       tag='pred',
       arity=2,
       rank=0,
       unpack={'x', 'y'},
       str='Q(x,y)',
       utf8='Q(x,y)',
       latex='Q(x,y)',
      },
      {expr='xgxRx',
       type='formula',
       tag='pred',
       arity=2,
       rank=0,
       unpack={'g(x,x)', 'x'},
       str='R(g(x,x),x)',
       utf8='xψxRx',
       latex='x\\psi{x}R{x}',
      },
      {expr={'A', '((A()))'},
       type='formula',
       tag='pred',
       arity=0,
       rank=0,
       unpack={},
       str='A',
       utf8='A',
       latex='A',
      },

-------- Formulas ----------------------------------------------------------
      {expr='¬A',
       type='formula',
       tag='not',
       arity=1,
       rank=1,
       unpack={'A'},
       str='(notA)',
       utf8='¬A',
       latex='\\lnot{A}',
      },
      {expr='\\lnot\\lnotA',
       type='formula',
       tag='not',
       arity=1,
       rank=2,
       unpack={'(notA)'},
       str='(not(notA))',
       utf8='¬¬A',
       latex='\\lnot\\lnot{A}',
      },
      {expr='notnotnotA',
       type='formula',
       tag='not',
       arity=1,
       rank=3,
       unpack={'(not(notA))'},
       str='(not(not(notA)))',
       utf8='¬¬¬A',
       latex='\\lnot\\lnot\\lnot{A}',
      },
      {expr='¬¬¬AandB',
       type='formula',
       tag='and',
       arity=2,
       rank=4,
       unpack={'(not(not(notA)))', 'B'},
       str='((not(not(notA)))andB)',
       utf8='¬¬¬A∧B',
       latex='\\lnot\\lnot\\lnot{A}\\land{B}',
      },
      {expr='A∧¬\\lnot¬B',
       type='formula',
       tag='and',
       arity=2,
       rank=4,
       unpack={'A', '(not(not(notB)))'},
       str='(Aand(not(not(notB))))',
       utf8='A∧¬¬¬B',
       latex='A\\land\\lnot\\lnot\\lnot{B}',
      },
      {expr='A∧B',
       type='formula',
       tag='and',
       arity=2,
       rank=1,
       unpack={'A', 'B'},
       str='(AandB)',
       utf8='A∧B',
       latex='A\\land{B}',
      },
      {expr='x=x∧x=x∧x=x',
       type='formula',
       tag='and',
       arity=2,
       rank=2,
       unpack={'x=x', '(x=xandx=x)'},
       str='(x=xand(x=xandx=x))',
       utf8='x=x∧x=x∧x=x',
       latex='x=x\\land{x=x\\land{x=x}}',
      },
      {expr='x=x∧¬(x=x∧(x=x))',
       type='formula',
       tag='and',
       arity=2,
       rank=3,
       unpack={'x=x', '(not(x=xandx=x))'},
       str='(x=xand(not(x=xandx=x)))',
       utf8='x=x∧¬(x=x∧x=x)',
       latex='x=x\\land\\lnot(x=x\\land{x=x})',
      },
      {expr='A∧B∧B∧A',
       type='formula',
       tag='and',
       arity=2,
       rank=3,
       unpack={'A', '(Band(BandA))'},
       str='(Aand(Band(BandA)))',
       utf8='A∧B∧B∧A',
       latex='A\\land{B\\land{B\\land{A}}}',
      },
      {expr='Aand((B))∨B',
       type='formula',
       tag='or',
       arity=2,
       rank=2,
       unpack={'(AandB)', 'B'},
       str='((AandB)orB)',
       utf8='A∧B∨B',
       latex='A\\land{B}\\lor{B}',
      },
      {expr='A∨¬A∧A',
       type='formula',
       tag='or',
       arity=2,
       rank=3,
       unpack={'A', '((notA)andA)'},
       str='(Aor((notA)andA))',
       utf8='A∨¬A∧A',
       latex='A\\lor\\lnot{A}\\land{A}',
      },
      {expr='A∧(B∨A)∧B',
       type='formula',
       tag='and',
       arity=2,
       rank=3,
       unpack={'A', '((BorA)andB)'},
       str='(Aand((BorA)andB))',
       utf8='A∧(B∨A)∧B',
       latex='A\\land(B\\lor{A})\\land{B}',
      },
      {expr='A∧(B∨B)∨B',
       type='formula',
       tag='or',
       arity=2,
       rank=3,
       unpack={'(Aand(BorB))', 'B'},
       str='((Aand(BorB))orB)',
       utf8='A∧(B∨B)∨B',
       latex='A\\land(B\\lor{B})\\lor{B}',
      },
      {expr='A∨¬A→A',
       type='formula',
       tag='to',
       arity=2,
       rank=3,
       unpack={'(Aor(notA))', 'A'},
       str='((Aor(notA))toA)',
       utf8='A∨¬A→A',
       latex='A\\lor\\lnot{A}\\to{A}',
      },
      {expr='¬(¬A→A)',
       type='formula',
       tag='not',
       arity=1,
       rank=3,
       unpack={'((notA)toA)'},
       str='(not((notA)toA))',
       utf8='¬(¬A→A)',
       latex='\\lnot(\\lnot{A}\\to{A})',
      },
      {expr='A↔¬(\\lnotA→A)',
       type='formula',
       tag='iff',
       arity=2,
       rank=4,
       unpack={'A', '(not((notA)toA))'},
       str='(Aiff(not((notA)toA)))',
       utf8='A↔¬(¬A→A)',
       latex='A\\iff\\lnot(\\lnot{A}\\to{A})',
      },
      {expr='∀xx=x',
       type='formula',
       tag='forall',
       arity=2,
       rank=1,
       unpack={'x', 'x=x'},
       str='(forallxx=x)',
       utf8='∀x(x=x)',
       latex='\\forall{x}(x=x)',
      },
      {expr='∀x¬x=x∧x=∅',
       type='formula',
       tag='and',
       arity=2,
       rank=3,
       unpack={'(forallx(notx=x))', 'x=empty'},
       str='((forallx(notx=x))andx=empty)',
       utf8='∀x(¬x=x)∧x=∅',
       latex='\\forall{x}(\\lnot{x=x})\\land{x=\\varnothing}',
      },
      {expr='∀xQffffxx',
       type='formula',
       tag='forall',
       arity=2,
       rank=1,
       unpack={'x', 'Q(f(f(f(f(x)))),x)'},
       str='(forallxQ(f(f(f(f(x)))),x))',
       utf8='∀xQ(φ(φ(φ(φ(x)))),x)',
       latex='\\forall{x}Q(\\varphi(\\varphi(\\varphi(\\varphi(x)))),x)',
      },
      {expr='\\existsx\\forallyy=y\\lornotx=a',
       type='formula',
       tag='or',
       arity=2,
       rank=3,
       unpack={'(existsx(forallyy=y))', '(notx=a)'},
       str='((existsx(forallyy=y))or(notx=a))',
       utf8='∃x∀y(y=y)∨¬x=α',
       latex='\\exists{x}\\forall{y}(y=y)\\lor\\lnot{x=\\alpha}',
      },
   }
   for i,t in ipairs (TESTS) do
      if type (t.expr) ~= 'table' then
         t.expr = {t.expr}
      end
      for _,s in ipairs (t.expr) do
         local assert = function (cond, fmt, ...)
            if cond then return cond end
            if fmt then
               fmt = ': '..fmt
            else
               fmt = ''
            end
            error (('failed on test #%d, expr="%s"%s')
                  :format (i, s, fmt:format (...)), 2)
         end
         local e, msg = C:parse (s)
         assert (e, msg)
         assert (e:type () == t.type, 'bad type: %s', e:type ())
         assert (e:tag () == t.tag, 'bad tag: %s', e:tag ())
         assert (e:arity () == t.arity, 'bad arity: %d', e:arity ())
         assert (e:rank () == t.rank, 'bad rank: %d', e:rank ())
         local u = {e:unpack ()}
         assert (#u == #t.unpack)
         for i,sub in ipairs (u) do
            assert (tostring (sub) == t.unpack[i],
                    'bad unpack: #%d, %s', i, tostring (sub))
            assert (sub:is_immediate_sub (e))
            local sub = assert (e:parse (t.unpack[i]))
            assert (sub:is_immediate_sub (e), 'not immediate sub: %s',
                    tostring (sub))
         end
         for _,sub in ipairs (t.proper or {}) do
            local sub = assert (e:parse (sub))
            assert (sub:is_proper_sub (e), 'not proper sub: %s',
                    tostring (sub))
         end
         assert (tostring (e) == t.str, 'bad str: %s', tostring (e))
         assert (e:typeset () == t.utf8, 'bad utf8: %s', e:typeset ())
         assert (e:typeset'latex' == t.latex,
                 'bad latex: %s', e:typeset'latex')
      end
   end
end
