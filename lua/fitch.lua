--[[ fitch.lua -- Fitch-style proofs in Lua.
     Copyright (C) 2016 PUC-Rio/Laboratorio TeleMidia

This file is part of Fitch.

Fitch is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

Fitch is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License
along with Fitch.  If not, see <http://www.gnu.org/licenses/>.  ]]--

local assert = assert
local error = error
local getmetatable = getmetatable
local io = io
local ipairs = ipairs
local pairs = pairs
local rawequal = rawequal
local setmetatable = setmetatable
local table = table
local tostring = tostring
local type = type

local print = print             -- debugging

local lpeg = require'lpeg'
local fitch = {}
_ENV = nil

---
-- Fitch objects: contexts, expressions, and trees.
-- @classmod fitch
---
do
   fitch.__index = fitch
end

-- Concatenates the contents of the given arrays into a new array.
local function xconcat (...)
   local args, t = {...}, {}
   for _,arg in ipairs (args) do
      for _,v in ipairs (arg) do
         table.insert (t, v)
      end
   end
   return t
end

-- Dumps the given object to stdout.
local function xdump (obj, excl, tab)
   local out = io.stdout
   if type (obj) ~= 'table' then
      out:write (tostring (obj))
   else
      local tab = tab or 1
      out:write ('{\n')
      for k,v in pairs (obj) do
         if excl and not excl[k] then
            out:write (('   '):rep (tab))
            out:write (k..'=')
            xdump (v, excl, tab + 1)
            out:write (',\n')
         end
      end
      out:write (('   '):rep (tab - 1)..'}')
   end
end

-- Inserts object into ordered array.
local function xinsert_ord (t, obj, cmp)
   local pos = 1
   while pos <= #t and cmp (obj, t[pos]) do
      pos = pos + 1
   end
   return table.insert (t, pos, obj)
end


-- Patterns.

local Space   = (lpeg.S ' \t\n')^0
local Open    = '(' * Space
local Close   = ')' * Space
local Comma   = ',' * Space
local Equals  = '=' * Space

local Digit   = lpeg.R'09'
local Lower   = lpeg.R'az'
local Upper   = lpeg.R'AZ'
local Alpha   = Lower + Upper
local Alnum   = Alpha + Digit
local SymId   = (lpeg.P'_' + Alpha) * (Alnum)^0

local Form    = lpeg.V'Form'
local FormIff = lpeg.V'FormIff'
local FormTo  = lpeg.V'FormTo'
local FormOr  = lpeg.V'FormOr'
local FormAnd = lpeg.V'FormAnd'
local FormQnt = lpeg.V'FormQnt'
local FormNot = lpeg.V'FormNot'
local Atom    = lpeg.V'Atom'
local PredEq  = lpeg.V'PredEq'
local Pred    = lpeg.V'Pred'
local PredIn  = lpeg.V'PredIn'
local Term1   = lpeg.V'Term1'
local Term2   = lpeg.V'Term2'
local Var     = lpeg.V'Var'
local Func    = lpeg.V'Func'
local FuncIn  = lpeg.V'FuncIn'

-- Logical connectors.
local Connectors = {
   ['iff']    = {infix=-6, utf8={'↔'}, latex={'\\iff', '\\leftrightarrow'}},
   ['to']     = {infix=-5, utf8={'→'}, latex={'\\to', '\\rightarrow'}},
   ['or']     = {infix=-4, utf8={'∨'}, latex={'\\lor', '\\vee'}},
   ['and']    = {infix=-3, utf8={'∧'}, latex={'\\land', '\\wedge'}},
   ['forall'] = {infix=-2, utf8={'∀'}, latex={'\\forall'}},
   ['exists'] = {infix=-2, utf8={'∃'}, latex={'\\exists'}},
   ['not']    = {infix=-1, utf8={'¬'}, latex={'\\lnot', '\\neg'}},
}
do
   for k,v in pairs (Connectors) do
      local pat = lpeg.P (k)
      for _,s in ipairs (xconcat (v.utf8, v.latex)) do
         pat = pat + lpeg.P (s)
      end
      Connectors[k].pat = lpeg.C (pat) * Space
   end
end
local ConnIff = Connectors.iff.pat
local ConnTo  = Connectors.to.pat
local ConnOr  = Connectors['or'].pat
local ConnAnd = Connectors['and'].pat
local ConnAll = Connectors.forall.pat
local ConnEx  = Connectors.exists.pat
local ConnNot = Connectors['not'].pat

-- Returns a function that wraps pattern captures into an object.
local function wrap (ctx, type, tag, style)
   if style == nil or style == 'prefix' then -- prefix operator
      return function (sym, ...)
         local t = {_ctx=ctx, _type=type, _tag=tag, _sym=sym, ...}
         return setmetatable (t, fitch)
      end
   elseif style == 'infix' then -- left-associative, infix operator
      local func
      func = function (x, sym, ...)
         if sym == nil then
            return assert (x)   -- nothing to do
         end
         assert (x and sym)
         local t = {_ctx=ctx, _type=type, _tag=tag, _sym=sym, x}
         setmetatable (t, fitch)
         table.insert (t, func (...))
         return t
      end
      return func
   else
      error'should not get here'
   end
end

-- Returns a pattern that matches N occurrences of TERM.
local function TermList (n, term)
   assert (n >= 0)
   if n == 0 then
      return (Open * Close)^-1
   end
   local pat1, pat2 = term, Open * term
   for i=2,n do
      pat1, pat2 = pat1 * term, pat2 * Comma * term
   end
   return pat1 + pat2 * Close
end


----------------------------------------------------------------------------
-- Object.
-- @section Object

-- Tests whether OBJ is a fitch object.
local function fitch_is_obj (obj)
   return getmetatable (obj) == fitch
end

---
-- Gets the type of object.
-- @return Object type: either 'context', 'formula', or 'term'.
---
function fitch:type ()
   return assert (self._type)
end

---
-- Gets the context associated with object.
-- @return Associated context.
---
function fitch:context ()
   return assert (self._ctx)
end

---
-- Dumps object to stdout.
---
function fitch:dump ()
   xdump (self, {_ctx=true,_type=true})
   io.stdout:write ('\n')
end

---
-- Tests whether object is a context.
-- @return[1] `true`, if object is a context.
-- @return[2] `false`, otherwise.
---
function fitch:is_context ()
   return self:type () == 'context'
end

---
-- Tests whether object is a formula.
-- @return[1] `true`, if object is a formula.
-- @return[2] `false`, otherwise.
---
function fitch:is_formula ()
   return self:type () == 'formula'
end

---
-- Tests whether object is a term.
-- @return[1] `true`, if object is a term.
-- @return[2] `false`, otherwise.
---
function fitch:is_term ()
   return self:type () == 'term'
end

---
-- Tests whether object is an expression (formula or term).
-- @return[1] `true`, if object is an expression.
-- @return[2] `false`, otherwise.
---
function fitch:is_expression ()
   local tp = self:type ()
   return tp == 'formula' or tp == 'term'
end

---
-- Tests whether object is equal to another.
-- @param other Object.
-- @return[1] `true`, if successful.
-- @return[2] `false`, otherwise.
---
function fitch:__eq (other)
   if not fitch_is_obj (self) or not fitch_is_obj (other) then
      return false
   end
   local tp1, tp2 = self:type (), other:type ()
   if tp1 ~= tp2 then
      return false
   end
   if tp1 == 'context' then
      return rawequal (self, other)
   end
   return fitch.__eq_expression (self, other)
end

---
-- Dumps object to string.
-- @return String representation of object.
---
function fitch:__tostring ()
   return fitch['__tostring_'..assert (self._type)] (self)
end


----------------------------------------------------------------------------
-- Context.
-- @section Context

---
-- Creates a new context.
-- @param[opt] spec Language specification, i.e., an array whose entries are
-- sub-arrays, each containing the arguments to a call to `fitch.install`.
-- @return A new context.
---
function fitch:new (spec)
   local obj = {}
   obj._type = 'context'        -- object type
   obj._ctx = obj               -- reference to self
   obj.funclist = {}            -- list of all function symbols
   obj.funcinlist = {}          -- list of infix function symbols
   obj.predlist = {}            -- list of all predicate symbols
   obj.predinlist = {}          -- list of infix predicate symbols
   obj.symbols = {}             -- symbol table
   obj.translate = {}           -- symbol translation table
   obj.G = {                    -- initial grammar
      Form,
      Form   = FormIff + Term1,
      FormIff= FormTo * (ConnIff * FormTo)^0
         / wrap (obj, 'formula', 'iff', 'infix'),
      FormTo = FormOr * (ConnTo * FormOr)^0
         / wrap (obj, 'formula', 'to', 'infix'),
      FormOr = FormAnd * (ConnOr * FormAnd)^0
         / wrap (obj, 'formula', 'or', 'infix'),
      FormAnd= FormQnt * (ConnAnd * FormQnt)^0
         / wrap (obj, 'formula', 'and', 'infix'),
      FormQnt= ConnEx * Var * FormQnt / wrap (obj, 'formula', 'exists')
         + ConnAll * Var * FormQnt / wrap (obj, 'formula', 'forall')
         + FormNot,
      FormNot= ConnNot * FormNot / wrap (obj, 'formula', 'not')
         + Atom + Open * Form * Close,
      Atom   = PredEq,
      PredEq = lpeg.Cc'=' * Term1 * Equals * Term1
         / wrap (obj, 'formula', 'eq'),
      Pred   = nil,             -- predicate symbols
      PredIn = nil,             -- infix predicate symbols
      Term1  = Term2,
      Term2  = Open * Term2 * Close + Var,
      Var    = lpeg.C (Alpha) * Space
         / wrap (obj, 'term', 'var'),
      Func   = nil,             -- function symbols
      FuncIn = nil,             -- infix function symbols
   }
   setmetatable (obj, fitch)
   local spec = spec or self or {}
   for _,v in ipairs (spec) do
      obj:install (table.unpack (v))
   end
   return obj
end

-- Gets the entry of symbol in context's symbol table, plus its id.
function fitch:_sym_info (sym)
   local ctx = self:context ()
   local sym = sym or assert (self._sym)
   local id = ctx.translate[sym]
   if not id then
      return nil
   end
   return ctx.symbols[id], id
end

-- Gets the pattern of symbol in context's symbol table.
function fitch:_sym_pattern (sym)
   local info = self:_sym_info (sym)
   return assert (info.pattern)
end

---
-- Gets the id of symbol.
-- @param[opt] sym The symbol id or one of its aliases.
-- @return Symbol id.
---
function fitch:sym_id (sym)
   local _, id = self:_sym_info (sym)
   return id
end

---
-- Gets the arity of symbol.
-- @param[opt] sym The symbol id or one of its aliases.
-- @return Symbol arity.
---
function fitch:sym_arity (sym)
   local info = self:_sym_info (sym)
   return assert (info.arity)
end

---
-- Gets the UTF-8 aliases of symbol.
-- @param[opt] sym The symbol id or one of its aliases.
-- @return Symbol UTF-8 aliases.
---
function fitch:sym_utf8 (sym)
   local info = self:_sym_info (sym)
   return table.unpack (info.utf8)
end

---
-- Gets the LaTeX aliases of symbol.
-- @param[opt] sym The symbol id or one of its aliases.
-- @return Symbol LaTeX aliases.
---
function fitch:sym_latex (sym)
   local info = self:_sym_info (sym)
   return table.unpack (info.latex)
end

---
-- Gets the infix-flag value associated with symbol.
-- @param[opt] sym The symbol id or one of its aliases.
-- @return Symbol infix-flag value.
---
function fitch:sym_flag_infix (sym)
   local info = self:_sym_info (sym)
   return info.flags.infix
end

---
-- Installs symbol into context.
-- @param id Symbol id.
-- @param tag Symbol tag: either 'func' or 'pred'.
-- @param arity Symbol arity.
-- @param[opt] utf8 UTF-8 aliases for *sym*.
-- @param[optchain] latex LaTeX aliases for *sym*.
-- @param[optchain] flags Extra flags.
-- @return Modified context.
---
function fitch:install (id, tag, arity, utf8, latex, flags)
   local ctx = assert (self._ctx)
   assert (lpeg.match (lpeg.C (SymId), id), 'bad symbol id: '..id)
   assert (tag == 'func' or tag == 'pred', 'bad symbol tag: '..tag)
   assert (arity >= 0, 'bad arity: '..arity)
   local utf8 = utf8 or {}
   local latex = latex or {}
   local flags = flags or {}
   local info = {tag=tag, arity=arity, utf8=utf8, latex=latex, flags={}}
   if arity == 2 and flags.infix then -- collect known flags
      info.flags.infix = flags.infix
   end
   local assert_not_installed = function (ctx, sym)
      assert (not ctx:sym_id (sym), 'symbol already installed: '..id)
   end
   local aliases = {}
   assert_not_installed (ctx, id)
   for _,s in ipairs (utf8) do
      assert_not_installed (ctx, s)
      table.insert (aliases, s)
   end
   for _,s in ipairs (latex) do
      assert_not_installed (ctx, s)
      table.insert (aliases, s)
   end
   local pat = lpeg.P (id)      -- update and install symbol info
   for _,s in ipairs (aliases) do
      ctx.translate[s] = id
      pat = pat + lpeg.P (s)
   end
   info.pattern = pat
   ctx.symbols[id] = info
   ctx.translate[id] = id
   table.insert (ctx[tag..'list'], id)
   local prefix = (lpeg.Cc (id) * pat) * Space -- update grammar
   if tag == 'func' then
      if ctx.G.Func == nil then
         ctx.G.Term2 = Open * Term1 * Close + Func + Var
         ctx.G.Func = prefix * TermList (arity, Term2)
            / wrap (ctx, 'term', tag)
      else
         ctx.G.Func = ctx.G.Func + prefix * TermList (arity, Term2)
            / wrap (ctx, 'term', tag)
      end
      if flags.infix then
         local list = ctx[tag..'inlist']
         local cmp = function (id1, id2)
            local x = ctx:sym_flag_infix (id1)
            local y = ctx:sym_flag_infix (id2)
            return x < y
         end
         xinsert_ord (list, id, cmp)
         if ctx.G.FuncIn == nil then
            ctx.G.Term1 = Open * Term2 * Close + FuncIn
         end
         ctx.G.FuncIn = lpeg.V ('FuncIn'..list[1])
         for i=1,#list-1 do     -- reinstall all infix function symbols
            local id, id_next = list[i], list[i+1]
            local pat = lpeg.Cc (id) * ctx:_sym_pattern (id) * Space
            local pat_next = lpeg.V ('FuncIn'..id_next)
            ctx.G['FuncIn'..id] = pat_next * (pat * pat_next)^0
               / wrap (ctx, 'term', tag, 'infix')
         end
         local id = list[#list]
         local pat = lpeg.Cc (id) * ctx:_sym_pattern (id) * Space
         ctx.G['FuncIn'..id] = Term2 * (pat * Term2)^0
            / wrap (ctx, 'term', tag, 'infix')
      end
   elseif tag == 'pred' then
      if ctx.G.Pred == nil then
         ctx.G.Atom = Pred + PredEq
         ctx.G.Pred = prefix * TermList (arity, Term1)
            / wrap (ctx, 'formula', tag)
      else
         ctx.G.Pred = ctx.G.Pred + prefix * TermList (arity, Term1)
            / wrap (ctx, 'formula', tag)
      end
      if flags.infix then
         if ctx.G.PredIn == nil then
            ctx.G.Atom = Pred
               + Term1 * PredIn * Space * Term1
               / wrap (ctx, 'formula', tag, 'infix')
               + PredEq
            ctx.G.PredIn = lpeg.Cc (id) * pat
         else
            ctx.G.PredIn = ctx.G.PredIn + lpeg.Cc (id) * pat
         end
      end
   else
      error'should not get here'
   end
   return ctx
end

---
-- Parses expression string.
-- @param s Expression string.
-- @return[1] Resulting expression.
-- @return[2] `nil` plus error message.
---
function fitch:parse (s)
   local e = lpeg.match (Space * assert (self._ctx).G * -1, s)
   if e == nil then
      return nil, 'syntax error'
   end
   return e
end

-- Metamethod: __tostring for contexts.
function fitch.__tostring_context (obj)
   local s = ''
   for _,k in ipairs {'func', 'pred'} do
      local list = k..'list'
      for _,id in ipairs (obj[list]) do
         s = s..id..'^'..obj:sym_arity (id)
         s = s..(obj:sym_flag_infix (id) and '*' or '')
         s = s..'\t'..k
         s = s..'\t'..table.concat ({obj:sym_utf8 (id)}, ',')
         s = s..'\t'..table.concat ({obj:sym_latex (id)}, ',')
         s = s..'\n'
      end
   end
   return s
end


----------------------------------------------------------------------------
-- Expression.
-- @section Expression

---
-- Gets the tag of expression.
-- @return Expression tag.
---
function fitch:tag ()
   return self._tag
end

---
-- Gets the arity of expression.
-- @return Expression arity.
---
function fitch:arity ()
   return #self
end

---
-- Tests whether expression is a variable.
-- @return[1] `true`, if expression is a variable.
-- @return[2] `false`, otherwise.
---
function fitch:is_variable ()
   return self:type () == 'term' and self:tag () == 'var'
end

---
-- Tests whether expression is a function.
-- @return[1] `true`, if expression is a function
-- @return[2] `false`, otherwise.
---
function fitch:is_function ()
   return self:type () == 'term' and self:tag () == 'func'
end

---
-- Tests whether expression is an equality predicate.
-- @return[1] `true`, if expression is an equality predicate.
-- @return[2] `false`, otherwise.
---
function fitch:is_equals ()
   return self:type () == 'formula' and self:tag () == 'eq'
end

---
-- Tests whether expression is a predicate.
-- @return[1] `true`, if expression is a predicate.
-- @return[2] `false`, otherwise.
---
function fitch:is_predicate ()
   return self:type () == 'formula' and self:tag () == 'pred'
end

---
-- Tests whether expression is an atomic formula.
-- @return[1] `true`, if expression is an atomic formula.
-- @return[2] `false`, otherwise.
---
function fitch:is_atom ()
   return self:is_equals () or self:is_predicate ()
end

---
-- Tests whether expression is a negation.
-- @return[1] `true`, if expression is a negation.
-- @return[2] `false`, otherwise.
---
function fitch:is_not ()
   return self:type () == 'formula' and self:tag () == 'not'
end

---
-- Tests whether expression is a quantifier.
-- @return[1] `true`, if expression is a quantifier.
-- @return[2] `false`, otherwise.
---
function fitch:is_quantifier ()
   if self:type () ~= 'formula' then
      return false
   end
   local tag = self:tag ()
   return tag == 'forall' or tag == 'exists'
end

---
-- Tests whether expression is a conjunction.
-- @return[1] `true`, if expression is a conjunction.
-- @return[2] `false`, otherwise.
---
function fitch:is_and ()
   return self:type () == 'formula' and self:tag () == 'and'
end

---
-- Tests whether expression is a disjunction.
-- @return[1] `true`, if expression is a disjunction.
-- @return[2] `false`, otherwise.
---
function fitch:is_or ()
   return self:type () == 'formula' and self:tag () == 'or'
end

---
-- Tests whether expression is an implication.
-- @return[1] `true`, if expression is a implication.
-- @return[2] `false`, otherwise.
---
function fitch:is_to ()
   return self:type () == 'formula' and self:tag () == 'to'
end

---
-- Tests whether expression is an bi-implication.
-- @return[1] `true`, if expression is a bi-implication.
-- @return[2] `false`, otherwise.
---
function fitch:is_iff ()
   return self:type () == 'formula' and self:tag () == 'iff'
end

---
-- Tests whether expression is an immediate subexpression of another.
-- @param other Expression.
-- @return[1] `true`, if expression is an immediate subexpression.
-- @return[2] `false`, otherwise.
---
function fitch:is_immediate_sub (other)
   for i=1,#other do
      if self == other[i] then
         return true, i
      end
   end
   return false
end

---
-- Tests whether expression is a proper subexpression of another.
-- @param other Expression.
-- @return[1] `true`, if expression is a proper subexpression.
-- @return[2] `false`, otherwise.
---
function fitch:is_proper_sub (other)
   if #other == 0 then
      return false
   end
   if self:is_immediate_sub (other) then
      return true
   end
   for i=1,#other do
      if self:is_proper_sub (other[i]) then
         return true
      end
   end
   return false
end

---
-- Gets the degree of precedence of expression.
-- @return Expression precedence degree.
---
function fitch:precedence ()
   if not self:is_expression () then
      return 0                  -- nothing to do
   end
   if self:is_atom () or self:is_variable () then
      return 0                  -- nothing to do
   end
   if self:is_term () then
      return -(self:sym_flag_infix () or 0)
   else
      assert (not self:is_atom ())
      return assert (Connectors[self._tag].infix)
   end
end

---
-- Gets the rank of expression.
-- @return Expression rank.
---
function fitch:rank ()
   if not self:is_expression () then
      return 0                  -- nothing to do
   end
   if self:is_term () or self:is_atom () then
      return 0
   end
   if self:is_not () then
      return self[1]:rank () + 1
   end
   if self:is_quantifier () then
      return self[2]:rank () + 1
   end
   local max = 0
   for i=1,#self do
      local n = self[i]:rank ()
      if n > max then
         max = n
      end
   end
   return max + #self - 1       -- compensate n-ary connectors
end

---
-- Gets the immediate subexpressions of expression.
-- @return Immediate subexpressions.
---
function fitch:unpack ()
   if not self:is_expression () then
      return nil                -- nothing to do
   end
   return table.unpack (self)
end

---
-- Typesets expression.
-- @param[opt] style Either 'utf8' (default) or 'latex'.
-- @return Resulting string.
-- @function fitch:typeset
---
local function brace (s)
   return '('..s..')'
end

local function cbrace (s)
   return (s:sub (1,1)):match'%a' and '{'..s..'}' or s
end

local fitch_typeset = {
   utf8 = {
      term_variable = function (v)
         return v
      end,
      term_0ary = function (op)
         return op
      end,
      term_infix = function (t1, op, t2)
         return t1..op..t2
      end,
      term_nary = function (op, ...)
         return op..'('..table.concat ({...}, ',')..')'
      end,
      term_equals = function (t1, t2)
         return t1..'='..t2
      end,
      form_not = function (op, f)
         return op..f
      end,
      form_quantifier = function (op, var, f)
         return op..var..f
      end,
      form_other = function (op, ...)
         return table.concat ({...}, op)
      end,
   },
   latex = {
      term_variable = true,
      term_0ary = true,
      term_infix = function (t1, op, t2)
         return t1..op..cbrace (t2)
      end,
      term_nary = true,
      term_equals = true,
      form_not = function (op, f)
         return op..cbrace (f)
      end,
      form_quantifier = function (op, var, f)
         return op..cbrace (var)..f
      end,
      form_other = function (op, t1, ...)
         local s, t = t1, {...}
         for i=1,#t do
            s = s..op..cbrace (t[i])
         end
         return s
      end
   }
}
do                              -- inherit undefined functions
   for k,_ in pairs (fitch_typeset.latex) do
      if fitch_typeset.latex[k] == true then
         fitch_typeset.latex[k] = assert (fitch_typeset.utf8[k])
      end
   end
end

function fitch:typeset (style)
   if not self:is_expression () then
      return nil                -- nothing to do
   end
   local style = style or 'utf8'
   local helper = fitch_typeset[style]
   if self:is_variable () then
      return helper.term_variable (self._sym)
   end
   if self:is_term () or self:is_predicate () then
      local sym = self['sym_'..style] (self) or self._sym
      if #self == 0 then
         return helper.term_0ary (sym)
      end
      if self:sym_flag_infix () then
         local t1 = self[1]:typeset (style)
         local t2 = self[2]:typeset (style)
         if self:is_term () then
            local p = self:precedence ()
            if p > self[1]:precedence () then
               t1 = brace (t1)
            end
            if p > self[2]:precedence () then
               t2 = brace (t2)
            end
         end
         return helper.term_infix (t1, sym, t2)
      else
         local t = {}
         for i=1,#self do
            table.insert (t, self[i]:typeset (style))
         end
         return helper.term_nary (sym, table.unpack (t))
      end
   end
   if self:is_equals () then
      local t1 = self[1]:typeset (style)
      local t2 = self[2]:typeset (style)
      return helper.term_equals (t1, t2)
   end
   -- Non-atomic formula.
   assert (not self:is_atom ())
   local tag = self._tag
   local sym = assert (Connectors[tag][style][1])
   if self:is_not () then
      local f = self[1]:typeset (style)
      if self:precedence () > self[1]:precedence () then
         f = brace (f)
      end
      return helper.form_not (sym, f)
   elseif self:is_quantifier () then
      local var = self[1]:typeset (style)
      local f = self[2]:typeset (style)
      if not self[2]:is_quantifier () and not self[2]:is_predicate () then
         f = brace (f)
      end
      return helper.form_quantifier (sym, var, f)
   else
      local f1 = self[1]:typeset (style)
      local f2 = self[2]:typeset (style)
      local p = self:precedence ()
      if p > self[1]:precedence () then
         f1 = brace (f1)
      end
      if p > self[2]:precedence () then
         f2 = brace (f2)
      end
      return helper.form_other (sym, f1, f2)
   end
   error'should not get here'
end

-- Metamethod: __eq for expressions.
function fitch:__eq_expression (other)
   if self._tag ~= other._tag or #self ~= #other then
      return false
   end
   if self._type == 'term' and self._sym ~= other._sym then
      return false
   end
   for i=1,#self do
      if self[i] ~= other[i] then
         return false
      end
   end
   return true
end

-- Metamethod: __tostring for formulas.
function fitch:__tostring_formula ()
   if self:is_equals () then
      return tostring (self[1])..'='..tostring (self[2])
   end
   if self:is_predicate () then
      return self:__tostring_term ()
   end
   if self:is_not () then
      return '('..self._tag..tostring (self[1])..')'
   end
   if self:is_quantifier () then
      return '('..self._tag..tostring (self[1])..tostring (self[2])..')'
   end
   assert (not self:is_atom ())
   return '('..tostring (self[1])..self._tag..tostring (self[2])..')'
end

-- Metamethod: __tostring for terms.
function fitch:__tostring_term ()
   if self:is_variable () then
      return self._sym
   end
   local sym = assert (self._ctx.translate[self._sym])
   if #self == 0 then
      return sym
   end
   local result = {}
   for _,s in ipairs (self) do
      table.insert (result, tostring (s))
   end
   return sym..'('..table.concat (result, ',')..')'
end

return fitch
