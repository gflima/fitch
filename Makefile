PDFLATEX= pdflatex
PDF= fitch.pdf
all: $(PDF)
clean:
	-rm -f $(PDF) $(PDF:.pdf=.log) $(PDF:.pdf=.aux)
.PHONY: all clean
.SUFFIXES: .tex .pdf
.tex.pdf:
	pdflatex -verbose -pdf $<
	pdflatex -verbose -pdf $<
